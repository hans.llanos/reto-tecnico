package com.retos.app.services;

import java.util.List;

import com.retos.app.model.Product;

public interface ProductService {
  
    List<Product>  save(Product product);
}
