package com.retos.app.services;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.retos.app.mappers.ProductMapper;
import com.retos.app.model.Product;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductMapper productMapper;

    

    @Override
    public List<Product> save(Product product) {
        Map<String, Object> params = new HashMap<String, Object>(); 
        ResultSet rs = null;
        
        params.put("product", product);
        params.put("products", rs);
        params.put("codigo", null);
        params.put("mensaje", null);

        productMapper.save(params);

        int codigo = Integer.parseInt((String)params.get("codigo"));
        String mensaje = (String) params.get("mensaje");        
        
        if(codigo!=0){
            throw new RuntimeException(mensaje);
        }
        
        List<Product> products = ((ArrayList<Product>)params.get("products"));
        return products;
    }

}
