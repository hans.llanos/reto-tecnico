package com.retos.app.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.retos.app.model.Product;
import com.retos.app.services.ProductService;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
public class ProductoController {
    @Autowired
    ProductService productService;

    @PostMapping("/products")
    public ResponseEntity<?> postProduct(@RequestBody Product entity) {
        try {
            List<Product> products = productService.save(entity);    

            return ResponseEntity.ok(Collections.singletonMap("products", products));

        } catch (Exception e) {

            return ResponseEntity.badRequest().body(Collections.singletonMap("mensaje", e.getMessage()));
        }
        

    }
    
    
}
