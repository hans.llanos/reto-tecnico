package com.retos.app.mappers;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.type.JdbcType;

import com.retos.app.model.Product;

@Mapper
public interface ProductMapper {
    @Results(id = "productResultMap", value = {
        @Result(property = "id", column = "ID", jdbcType = JdbcType.INTEGER),
        @Result(property = "nombre", column = "NOMBRE", jdbcType = JdbcType.VARCHAR),
        @Result(property = "fecRegistro", column = "FEC_REGISTRO", jdbcType = JdbcType.TIMESTAMP),
    })
    @ConstructorArgs({
        @Arg(column = "ID", javaType = Integer.class)
        ,@Arg(column = "NOMBRE", javaType = String.class)
        ,@Arg(column = "FEC_REGISTRO", javaType = Timestamp.class)
    })
    @Select(value= "{ CALL PCK_PRODUCTOS.SP_EJEMPLO(#{product.id, mode=IN, jdbcType=INTEGER}, #{product.nombre, mode=IN, jdbcType=VARCHAR}, #{product.fecRegistro, mode=IN, jdbcType=TIMESTAMP}, #{products, mode=OUT, jdbcType=CURSOR, javaType=ResultSet, resultMap=productResultMap}, #{codigo, mode=OUT, jdbcType=VARCHAR}, #{mensaje, mode=OUT, jdbcType=VARCHAR})}")
    @Options(statementType = StatementType.CALLABLE)
    List<Product> save(Map<String, Object> params);
}
