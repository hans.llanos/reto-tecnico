package com.retos.app.model;


import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Product {

    public Integer id;	
	public String nombre;	
	public Timestamp fecRegistro;


}
